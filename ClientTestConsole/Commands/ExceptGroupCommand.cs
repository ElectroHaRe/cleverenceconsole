﻿using ClientTestConsole.Base;
using CommandSystem;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("ExceptGroup")]
    class ExceptGroupCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            Program.TestHandler.RemoveTests((attribute) => Args.Contains(attribute.Group));
            return true;
        }
    }
}
