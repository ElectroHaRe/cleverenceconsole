﻿using ClientTestConsole.Base;
using CommandSystem;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("ExceptTest")]
    public class ExceptCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            Program.TestHandler.RemoveTests((attribute) => Args.Contains(attribute.Name));
            return true;
        }
    }
}
