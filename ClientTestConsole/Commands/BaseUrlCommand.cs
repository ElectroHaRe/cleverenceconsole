﻿using Cleverence;
using ClientTestConsole.Base;
using CommandSystem;
using System.Configuration;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("BaseUrl", AlwaysRun = true)]
    public class BaseUrlCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            Client.BaseUrl = Args.Count == 0 ?
                ConfigurationManager.AppSettings.Get("BaseUrl") : Args[0];
            return true;
        }
    }
}
