﻿using ClientTestConsole.Base;
using CommandSystem;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("AddGroup")]
    class AddGroupCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            if (Args.Count == 0)
                return true;

            if (Args.Contains("All"))
            {
                Program.TestHandler.AddTests((attribute) => true);
                return true;
            }

            Program.TestHandler.AddTests((attribute) => Args.Contains(attribute.Group));
            return true;
        }
    }
}
