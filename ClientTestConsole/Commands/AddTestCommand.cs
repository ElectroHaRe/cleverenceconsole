﻿using ClientTestConsole.Base;
using CommandSystem;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("AddTest")]
    public class AddTestCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            if (Args.Count == 0)
                return true;

            if (Args.Contains("All"))
            {
                Program.TestHandler.AddTests((attribute) => true);
                return true;
            }

            Program.TestHandler.AddTests((attribute) => Args.Contains(attribute.Name));
            return true;
        }
    }
}
