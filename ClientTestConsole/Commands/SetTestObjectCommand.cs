﻿using ClientTestConsole.Base;
using CommandSystem;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("SetTestObject")]
    class SetTestObjectCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            for (int i = 0; i < Args.Count - 1; i++)
            {
                if (Args.Count <= i + 1)
                    continue;

                var tests = Program.TestHandler.GetTests((attribute) => attribute.Name == Args[i]);
                if (tests.Count != 0)
                    tests[Args[i]].SetArgs(Args[++i]);
            }
            return true;
        }
    }
}
