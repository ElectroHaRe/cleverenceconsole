﻿using CommandSystem;
using System.Linq;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("RunTests")]
    public class RunTestsCommand : Command
    {
        public override async Task<bool> Run() => 
            !(await Program.TestHandler.RunCommands()).Any((item) => !item.Value);
    }
}
