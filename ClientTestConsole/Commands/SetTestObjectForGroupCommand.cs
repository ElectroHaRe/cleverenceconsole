﻿using ClientTestConsole.Base;
using CommandSystem;
using System.Threading.Tasks;

namespace ClientTestConsole.Commands
{
    [Command("SetTestObjectForGroup")]
    public class SetTestObjectForGroupCommand : ClientCommand
    {
#pragma warning disable
        public override async Task<bool> Run()
        {
            for (int i = 0; i < Args.Count - 1; i++)
            {
                if (Args.Count <= i + 1)
                    continue;

                var tests = Program.TestHandler.GetTests((attribute) => attribute.Group == Args[i]);
                if (tests.Count != 0)
                    tests[Args[i]].SetArgs(Args[++i]);
            }
            return true;
        }
    }
}
