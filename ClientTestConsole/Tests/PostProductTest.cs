﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("PostProduct", Group = "Product")]
    public class PostProductTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var product = Args.Count == 0 ? GetObject<Product>(Properties.Resources.Product, false) :
                GetObject<Product>(Args[0]);

            return await ProductsV1_PostAsync_ProductCreated(product);
        }

        private async Task<bool> ProductsV1_PostAsync_ProductCreated(Product product)
        {
            if (await CheckProductById(product.Id))
            {
                Logger?.Error($"({this.GetType()}.ProductsV1_PostAsync_ProductCreated | " +
                    $"Product id: {product.Id}) Error: The product already exists");
                return false;
            }

            await Client.BeginUpdateProductsAsync(string.Empty);
            try { await Client.PostProductsAsync(product); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.ProductsV1_PostAsync_ProductCreated | " +
                    "Product id: {id}) Error: {@ex}", this.GetType(), product.Id, ex);
                return false;
            }
            finally { await Client.EndUpdateProductsAsync(string.Empty); }

            if (!await CheckProductById(product.Id))
            {
                Logger?.Error($"({this.GetType()}.ProductsV1_PostAsync_ProductCreated | " +
                    $"Product id: {product.Id}) Error: The product wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.ProductsV1_PostAsync_ProductCreated | Product id: {product.Id}");
            Logger?.Information("Result: {@result}", product);
#endif
            await Client.BeginUpdateProductsAsync(string.Empty);
            try { await Client.DeleteProductsAsync(product.Id, string.Empty); }
            finally { await Client.EndUpdateProductsAsync(string.Empty); }
#if DEBUG
            if (!await CheckProductById(product.Id))
                Logger?.Information("--The test product was delete");
            else Logger?.Error("Error: The test product wasn't delete.");
#endif
            return true;
        }

        private async Task<bool> CheckProductById(string id)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check product with id: {id}");
#endif
            try { await Client.ProductsV1_GetByIdAsync(id, null, null); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information("Result: doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.ProductsV1_GetByIdAsync | Product id: {id}) Error: {@ex}", this.GetType(), id, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }
    }
}
