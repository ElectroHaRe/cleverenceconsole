﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("DeleteWarehouse", Group = "Warehouse")]
    public class DeleteWarehouseTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var warehouse = Args.Count == 0 ? GetObject<Warehouse>(Properties.Resources.Warehouse, false) :
                GetObject<Warehouse>(Args[0]);

            return await WarehousesV1_DeleteByIdAsync_WarehouseDeleted(warehouse);
        }

        public async Task<bool> WarehousesV1_DeleteByIdAsync_WarehouseDeleted(Warehouse warehouse)
        {
            if (!await CheckWarehouseById(warehouse.Id))
                await TryPostWarehouseAsync(warehouse);

            try { await Client.DeleteWarehouseAsync(warehouse.Id, null); }
            catch (Exception ex)
            {
                Logger?.Information("({module}.WarehousesV1_DeleteByIdAsync_WarehouseDeleted | " +
                    "Warehouse id: {id}) Error: {@ex})", this.GetType(), warehouse.Id, ex);
                return false;
            }

            if (await CheckWarehouseById(warehouse.Id))
            {
                Logger?.Error($"({this.GetType()}).WarehousesV1_DeleteByIdAsync_WarehouseDeleted | " +
                    $"Warehouse id: {warehouse.Id}) Error: The warehouse wasn't delete");
                return false;
            }
#if DEBUG
            Logger?.Information($"({this.GetType()}.WarehousesV1_DeleteByIdAsync_WarehouseDeleted | Warehouse id: {warehouse.Id})");
            Logger?.Information("Result: The warehouse was delete\n{@result}", warehouse);
#endif
            return true;
        }

        private async Task<bool> CheckWarehouseById(string id)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check warehouse with id: {id}");
#endif
            try { await Client.WarehousesV1_GetByIdAsync(id, null, null); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information("Result: doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.WarehousesV1_GetByIdAsync | Warehouse id: {id}) Error: {@ex}", this.GetType(), id, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }

        private async Task<Warehouse> TryPostWarehouseAsync(Warehouse warehouse)
        {
            try { return await Client.PostWarehouseAsync(warehouse); }
            catch (ApiException ex) when (ex.StatusCode == 201) { return warehouse; }
        }
    }
}
