﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("DeleteProduct", Group = "Product")]
    public class DeleteProductTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var product = Args.Count == 0 ? GetObject<Product>(Properties.Resources.Product, false) :
                GetObject<Product>(Args[0]);

            return await ProductsV1_DeleteByIdAsync_ProductDeleted(product);
        }

        private async Task<bool> ProductsV1_DeleteByIdAsync_ProductDeleted(Product product)
        {
            if (!await CheckProductById(product.Id))
            {
                await Client.BeginUpdateProductsAsync(string.Empty);
                try { await Client.PostProductsAsync(product); }
                finally { await Client.EndUpdateProductsAsync(string.Empty); }
            }

            await Client.BeginUpdateProductsAsync(string.Empty);
            try { await Client.DeleteProductsAsync(product.Id, string.Empty); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.ProductsV1_DeleteByIdAsync_ProductDeleted  | " +
                    "Product id: {id}) Error: {@ex}", this.GetType(), product.Id, ex);
                return false;
            }
            finally { await Client.EndUpdateProductsAsync(string.Empty); }

            if (await CheckProductById(product.Id))
            {
                Logger?.Error($"({this.GetType()}.ProductsV1_DeleteByIdAsync_ProductDeleted  |" +
                    $" Product id: {product.Id}) Error: The product wasn't delete");
                return false;
            }
#if DEBUG
            Logger?.Information($"({this.GetType()}.ProductsV1_DeleteByIdAsync_ProductDeleted  | " +
                $"Product id: {product.Id}) Result: The product was delete");
#endif
            return true;
        }

        private async Task<bool> CheckProductById(string id)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check product with id: {id}");
#endif
            try { await Client.ProductsV1_GetByIdAsync(id, null, null); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information("Result: doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.ProductsV1_GetByIdAsync | Product id: {id}) Error: {@ex}", this.GetType(), id, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }
    }
}
