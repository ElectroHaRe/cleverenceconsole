﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("DeleteSerii", Group = "Serii")]
    public class DeleteSeriiTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var row = Args.Count == 0 ? GetObject<Serii>(Properties.Resources.Serii, false) :
                GetObject<Serii>(Args[0]);

            return await TablesV1_DeleteByUid_12Async_RowDeleted(row);
        }

        private async Task<bool> TablesV1_DeleteByUid_12Async_RowDeleted(Serii row)
        {
            if ((await GetSeriiById(row.Id))?.Value.Count == 0)
            {
                await Client.TablesV1_BeginUpdate_12Async(string.Empty);
                try { await Client.TablesV1_Post_12Async(row); }
                finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }
            }

            try { row = (await GetSeriiById(row.Id)).Value.Last(); }
            catch
            {
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_12Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't found");
                return false;
            }

            await Client.TablesV1_BeginUpdate_12Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_12Async(GetCorrectUid(row.Uid), string.Empty); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_DeleteByUid_12Async_RowDeleted  | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }

            try
            {
                await Client.TablesV1_GetSubByUid_12Async(row.Uid, null, null);
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_12Async_RowDeleted  | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't delete");
                return false;
            }
            catch (ApiException) { }
#if DEBUG
            Logger?.Information($"({this.GetType()}.TablesV1_DeleteByUid_12Async_RowDeleted  | Row uid: {row.Uid})");
            Logger?.Information("Result: The row was delete\n{@result}", row);
#endif
            return true;
        }

        private async Task<ODataResponseOfListOfSerii> GetSeriiById(string id)
        {
            try { return await Client.TablesV1_GetSub_12Async(null, $"id eq '{id}'", null, null, null, null, null); }
            catch { return null; }
        }

        private async Task<bool> CheckSeriiByUid(string uid)
        {
            uid = GetCorrectUid(uid);
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check serii with uid: {uid}");
#endif
            try { await Client.TablesV1_GetSubByUid_12Async(uid, null, null); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information($"Result: Uid {uid} doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.TablesV1_GetSubByUid_12Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information($"Result: Uid {uid} exists");
#endif
            return true;
        }
    }
}
