﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("PostKontragenty", Group = "Kontragenty")]
    public class PostKontragentyTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var kontragenty = Args.Count == 0 ? GetObject<Kontragenty>(Properties.Resources.Kontragenty, false) :
                GetObject<Kontragenty>(Args[0]);

            return await TablesV1_Post_7Async_RowCreated(kontragenty);
        }

        private async Task<bool> TablesV1_Post_7Async_RowCreated(Kontragenty row)
        {
            if (await CheckKontragentyByUid(row.Uid))
            {
                Logger?.Error($"({this.GetType()}.TablesV1_Post_7Async_RowCreated | " +
                    $"Row uid: {row.Uid}) Error: The row already exists");
                return false;
            }

            await Client.BeginUpdateRezervAsync(string.Empty);
            try { await TryPostKontragentyAsync(row); }
            catch (Exception ex)
            {
                await Client.EndUpdateRezervAsync(string.Empty);
                Logger?.Error("({module}.TablesV1_Post_7Async_RowCreated | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.EndUpdateRezervAsync(string.Empty); }

            if (!await CheckKontragentyByUid(row.Uid))
            {
                Logger?.Error($"({this.GetType()}.TablesV1_Post_7Async_RowCreated | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.TablesV1_Post_7Async_RowCreated) | Row uid: {row.Uid}");
            Logger?.Information("Result: {@result}", row);
#endif
            await Client.BeginUpdateRezervAsync(string.Empty);
            try { await Client.DeleteRezervAsync(row.Uid, string.Empty); }
            finally { await Client.EndUpdateRezervAsync(string.Empty); }
#if DEBUG
            if (!await CheckKontragentyByUid(row.Uid))
                Logger?.Information("--The test row was delete--");
            else Logger?.Error("Error: The test row wasn't delete");
#endif
            return true;
        }

        private async Task<bool> CheckKontragentyByUid(string uid)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check kontragenty with uid: {uid}");
#endif
            try { await Client.TablesV1_GetSubByUid_7Async(uid, null, null); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information("Result: doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module}.Client.TablesV1_GetSubByUid_7Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }

        private async Task<Kontragenty> TryPostKontragentyAsync(Kontragenty row)
        {
            try { return await Client.TablesV1_Post_7Async(row); }
            catch (ApiException ex) when (ex.StatusCode == 201) { return row; }
        }
    }
}
