﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("DeleteTransportnyeUpakovki", Group = "TransportnyeUpakovki")]
    public class DeleteTransportnyeUpakovkiTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var row = Args.Count == 0 ? GetObject<TransportnyeUpakovki>(Properties.Resources.TransportnyeUpakovki, false) :
                GetObject<TransportnyeUpakovki>(Args[0]);

            return await TablesV1_DeleteByUid_14Async_RowDeleted(row);
        }

        private async Task<bool> TablesV1_DeleteByUid_14Async_RowDeleted(TransportnyeUpakovki row)
        {
            if ((await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Count == 0)
            {
                await Client.TablesV1_BeginUpdate_14Async(string.Empty);
                try { await TryPostTransportnyeUpakovkiAsync(row); }
                finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }
            }

            try { row = (await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Last(); }
            catch
            {
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_14Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't found");
                return false;
            }

            await Client.TablesV1_BeginUpdate_14Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_14Async(GetCorrectUid(row.Uid), string.Empty); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_DeleteByUid_14Async_RowDeleted | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }

            try
            {
                await Client.TablesV1_GetSubByUid_14Async(row.Uid, null, null);
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_14Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't delete");
                return false;
            }
            catch (ApiException) { }
#if DEBUG
            Logger?.Information($"({this.GetType()}.TablesV1_DeleteByUid_14Async_RowDeleted | Row uid: {row.Uid}");
            Logger?.Information("Result: The row was delete\n{@result}", row);
#endif
            return true;
        }

        private async Task<ODataResponseOfListOfTransportnyeUpakovki> GetUpakovkiById(string id)
        {
            try { return await Client.TablesV1_GetSub_14Async(null, $"IdTransportnojUpakovki eq '{id}'", null, null, null, null, null); }
            catch { return null; }
        }

        private async Task<TransportnyeUpakovki> TryPostTransportnyeUpakovkiAsync(TransportnyeUpakovki row)
        {
            try { return await Client.TablesV1_Post_14Async(row); }
            catch (ApiException ex) when (ex.StatusCode == 201) { return row; }
        }
    }
}
