﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("PostTransportnyeUpakovki", Group = "TransportnyeUpakovki")]
    public class PostTransportnyeUpakovkiTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var row = Args.Count == 0 ? GetObject<TransportnyeUpakovki>(Properties.Resources.TransportnyeUpakovki, false) :
                GetObject<TransportnyeUpakovki>(Args[0]);

            return await TablesV1_Post_14Async_RowCreated(row);
        }

        private async Task<bool> TablesV1_Post_14Async_RowCreated(TransportnyeUpakovki row)
        {
            TransportnyeUpakovki lastRow = null;
            try { lastRow = (await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Last(); }
            catch { }

            await Client.TablesV1_BeginUpdate_14Async(string.Empty);
            try { await TryPostTransportnyeUpakovkiAsync(row); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_Post_14Async_RowCreated | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }

            try
            {
                row = (await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Last();
                if (row.Uid == lastRow?.Uid)
                    throw new Exception();
            }
            catch
            {
                Logger?.Information($"({this.GetType()}.TablesV1_Post_14Async_RowCreated | " +
                    $"Row id: {row.IdTransportnojUpakovki}) Error: The row wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.TablesV1_Post_14Async_RowCreated | Row uid: {row.Uid}");
            Logger?.Information("Result: The row was create\n{@result}", row);
#endif
            await Client.TablesV1_BeginUpdate_14Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_14Async(GetCorrectUid(row.Uid), string.Empty); }
            finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }
#if DEBUG
            if (!await CheckUpakovkiByUid(row.Uid))
                Logger?.Information("--The test row was delete--");
            else Logger?.Error("Error: The test row wasn't delete");
#endif
            return true;
        }

        private async Task<ODataResponseOfListOfTransportnyeUpakovki> GetUpakovkiById(string id)
        {
            try { return await Client.TablesV1_GetSub_14Async(null, $"IdTransportnojUpakovki eq '{id}'", null, null, null, null, null); }
            catch { return null; }
        }

        private async Task<bool> CheckUpakovkiByUid(string uid)
        {
            uid = GetCorrectUid(uid);
#if DEBUG
            Logger?.Information($"({this.GetType()}) Chek TransportnyeUpakovki with uid: {uid}");
#endif
            try { await Client.GetTransportnyeUpakovkiAsync(uid); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information($"Result: Uid {uid} doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.TablesV1_GetSubByUid_14Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }

#if DEBUG
            Logger?.Information($"Result: Uid {uid} exists");
#endif
            return true;

            string GetCorrectUid(string uidFromServer) => Regex.Match(uidFromServer, @"[0-9]+$").ToString();
        }

        private async Task<TransportnyeUpakovki> TryPostTransportnyeUpakovkiAsync(TransportnyeUpakovki row)
        {
            try { return await Client.PostTransportnyeUpakovkiAsync(row); }
            catch (ApiException ex) when (ex.StatusCode == 201) { return row; }
        }
    }
}
