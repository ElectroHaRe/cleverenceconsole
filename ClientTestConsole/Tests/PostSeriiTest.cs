﻿using Cleverence;
using ClientTestConsole.Base;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClientTestConsole.Tests
{
    [ClientTest("PostSerii", Group = "Serii")]
    public class PostSeriiTest : ClientCommand
    {
        public override async Task<bool> Run()
        {
            var row = Args.Count == 0 ? GetObject<Serii>(Properties.Resources.Serii, false) :
                GetObject<Serii>(Args[0]);

            return await TablesV1_Post_12Async_RowCreated(row);
        }

        private async Task<bool> TablesV1_Post_12Async_RowCreated(Serii row)
        {
            Serii lastRow = null;
            try { lastRow = (await GetSeriiById(row.Id))?.Value.Last(); }
            catch { }

            await Client.TablesV1_BeginUpdate_12Async(string.Empty);
            try { await Client.TablesV1_Post_12Async(row); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_Post_12Async_RowCreated | Row uid: {uid}) Error: {@ex}", this.GetType(), row.Id, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }

            try
            {
                row = (await GetSeriiById(row.Id)).Value.Last();
                if (row.Uid == lastRow?.Uid)
                    throw new Exception();
            }
            catch
            {
                Logger?.Error($"({this.GetType()}.TablesV1_Post_12Async_RowCreated | Row id: {row.Id}) Error: The row wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.TablesV1_Post_12Async_RowCreated |  Row uid: {row.Uid}");
            Logger?.Information("Result: The row was create\n{@result}", row);
#endif
            await Client.TablesV1_BeginUpdate_12Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_12Async(GetCorrectUid(row.Uid), string.Empty); }
            finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }
#if DEBUG
            if (!await CheckSeriiByUid(row.Uid))
                Logger?.Information("--The test row was delete--");
            else Logger?.Error("Error: The test row wasn't delete.");
#endif
            return true;
        }

        private async Task<ODataResponseOfListOfSerii> GetSeriiById(string id)
        {
            try { return await Client.TablesV1_GetSub_12Async(null, $"id eq '{id}'", null, null, null, null, null); }
            catch { return null; }
        }

        private async Task<bool> CheckSeriiByUid(string uid)
        {
            uid = GetCorrectUid(uid);
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check serii with uid: {uid}");
#endif
            try { await Client.TablesV1_GetSubByUid_12Async(uid, null, null); }
            catch (ApiException ex) when (ex.StatusCode == 404)
            {
#if DEBUG
                Logger?.Information($"Result: Uid {uid} doesn't exist");
#endif
                return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.TablesV1_GetSubByUid_12Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information($"Result: Uid {uid} exists");
#endif
            return true;
        }
    }
}
