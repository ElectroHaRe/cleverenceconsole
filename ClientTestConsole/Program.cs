﻿using Cleverence;
using ClientTestConsole.Base;
using Serilog;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientTestConsole
{
    static class Program
    {
        static Client client;
        static ILogger logger;

        public static ClientTestHandler TestHandler;

        static Program()
        {
            logger = new LoggerConfiguration()
#if DEBUG
                .MinimumLevel.Verbose()
#else
                .MinimumLevel.Error()
#endif
                .WriteTo.Console().CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromMinutes(4) });
            TestHandler = new ClientTestHandler(client, logger);
        }

        static async Task<int> Main(string[] args)
        {
#if DEBUG
            Console.WriteLine("args:");
            foreach (var item in args)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("----------");
            Console.WriteLine();
#endif
            var commandHandler = new ClientCommandHandler(client, args);
#if DEBUG
            TestHandler.OnCommandStart += TestHandler_OnCommandStart;
            commandHandler.OnCommandStart += CommandHandler_OnCommandStart;
            TestHandler.OnCommandFinish += TestHandler_OnCommandFinish;
#endif
            var results = await commandHandler.RunCommands();
#if DEBUG
            Console.WriteLine(results.Any((item) => !item.Value)? false : true);
            Console.ReadLine();
#endif
            if (results.Any((item) => !item.Value) ? false : true)
                return 0;
            else return 1;
        }

#if DEBUG
        private static void CommandHandler_OnCommandStart(string name, CommandSystem.Command command)
        {
            Console.WriteLine();
            Console.WriteLine($"--{name}--");
            Console.WriteLine();
        }

        private static void TestHandler_OnCommandFinish(string name, CommandSystem.Command command, bool result)
        {
            Console.WriteLine($"Result: {result}");
        }

        private static void TestHandler_OnCommandStart(string name, CommandSystem.Command command)
        {
            Console.WriteLine();
            Console.WriteLine($"--{name}--");
            Console.WriteLine();
        }
#endif
    }
}
