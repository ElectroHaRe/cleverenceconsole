﻿using Cleverence;
using CommandSystem;
using Serilog;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace ClientTestConsole.Base
{
    public abstract class ClientCommand : Command
    {
        public Client Client;
        public ILogger Logger;

        public ClientCommand(Client client = null, ILogger logger = null)
        {
            Client = client;
            Logger = logger;
        }

        protected string GetCorrectUid(string uidFromServer) =>
            Regex.Match(uidFromServer, @"[0-9]+$").ToString();

        private static JsonSerializerOptions options = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
        protected T GetObject<T>(string inputJson, bool isPath = true)
        {
            string json = string.Empty;
            if (isPath)
            {
                var rootExpr = @"^[\\]{0,1}[\.]{2}[\\]{1}";

                if (!inputJson.Contains(@"\"))
                    inputJson = Path.Combine(Directory.GetCurrentDirectory(), inputJson);

                if (Regex.IsMatch(inputJson, rootExpr))
                    inputJson = Path.Combine(Directory.GetCurrentDirectory(), inputJson);

                json = File.ReadAllText(inputJson.ToString());
            }
            else json = inputJson;
            return JsonSerializer.Deserialize<T>(json, options);
        }
    }
}
