﻿using Cleverence;
using CommandSystem;
using Serilog;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ClientTestConsole.Base
{
    public class ClientTestHandler : ClientCommandHandler
    {
        private Dictionary<string, Command> allTests = new Dictionary<string, Command>();

        public ClientTestHandler(Client client, ILogger logger = null)
            : base(client, logger) { Preload(); }

        public void AddTests(Predicate<ClientTestAttribute> selector)
        {
            foreach (var test in allTests)
            {
                if (selector(test.Value.GetType().GetCustomAttribute<ClientTestAttribute>()))
                    commands.Enqueue(test);
            }
        }

        public void RemoveTests(Predicate<ClientTestAttribute> selector)
        {
            foreach (var test in commands.ToArray())
            {
                if (selector(test.Value.GetType().GetCustomAttribute<ClientTestAttribute>()))
                    commands.Dequeue();
                else commands.Enqueue(commands.Dequeue());
            }
        }

        public Dictionary<string, Command> GetTests(Predicate<ClientTestAttribute> selector)
        {
            var temp = new Dictionary<string, Command>();
            foreach (var test in commands)
            {
                if (selector(test.Value.GetType().GetCustomAttribute<ClientTestAttribute>()))
                    if (!temp.ContainsKey(test.Key))
                        temp.Add(test.Key, test.Value);
            }
            return temp;
        }

        private void Preload()
        {
            InitializeCommands(All);
            foreach (var command in commands)
            {
                allTests.Add(command.Key, command.Value);
            }
            commands.Clear();
        }

        protected override bool AttributeSelector(CommandAttribute attribute)
        {
            return attribute != null && attribute is ClientTestAttribute;
        }
    }
}
