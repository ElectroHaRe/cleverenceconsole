﻿using System;
using CommandSystem;

namespace ClientTestConsole.Base
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ClientTestAttribute : CommandAttribute
    {
        public string Group { get; set; }

        public ClientTestAttribute(string name) : base(name) { }
    }
}
