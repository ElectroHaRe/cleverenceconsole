﻿using Cleverence;
using CommandSystem;
using Serilog;

namespace ClientTestConsole.Base
{
    public class ClientCommandHandler : CommandHandler
    {
        public readonly Client Client;
        public readonly ILogger Logger;

        public ClientCommandHandler(Client client, params string[] input)
        {
            Client = client;
            InitializeCommands(input);
        }

        public ClientCommandHandler(Client client, ILogger logger, params string[] input)
        {
            Client = client;
            Logger = logger;
            InitializeCommands(input);
        }

        protected override void SetupCommand(Command command, params string[] args)
        {
            base.SetupCommand(command, args);
            if (!(command is ClientCommand))
                return;

            (command as ClientCommand).Client = Client;
            (command as ClientCommand).Logger = Logger;
        }
    }
}
