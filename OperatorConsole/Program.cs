﻿using Cleverence;
using OperatorConsole.Base;
using Serilog;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OperatorConsole
{
    class Program
    {
        private static ILogger logger;
        private static Client client;
        private static ClientCommandHandler commandHandler;
        private static string currentReport = string.Empty;

        static Program()
        {
            string logPath = ConfigurationManager.AppSettings.Get("LogPath");
            if (!Path.IsPathRooted(logPath))
                logPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), logPath);
            logger = new LoggerConfiguration().WriteTo
                .File(logPath, rollingInterval: RollingInterval.Day,
                flushToDiskInterval: TimeSpan.FromDays(7)).CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromMinutes(4) });
            client.BaseUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
        }

        static async Task<int> Main(string[] args)
        {
            try
            {
                Console.OutputEncoding = Encoding.UTF8;
                commandHandler = new ClientCommandHandler(client, logger, args);

                commandHandler.OnReportUpdated += UpdateReport;
                commandHandler.OnReportUpdated += UpdateReport;
                commandHandler.OnCommandStart +=
                    (name, command) => logger.Information("{command} start | args: {@args}", name, command.GetArgs());
                commandHandler.OnCommandFinish +=
                    (name, command, result) => logger.Information("{command} finish | result: {result}", name, result);

                var results = await commandHandler.RunCommands();
#if DEBUG
                Console.ReadLine();
#endif
                if (results.Any((item) => !item.Value) ? false : true)
                    return 0;
                else return 1;
            }
            catch (Exception ex)
            {
                logger.Error("{@ex}", ex);
                throw ex;
            }
        }

        private static void UpdateReport(string report)
        {
            var info = report.Remove(0, currentReport.Length);
            if (string.IsNullOrEmpty(info))
                return;
            Console.Write(report.Remove(0, currentReport.Length));
            currentReport = commandHandler.Report;
        }
    }
}
