﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Text;
using System.Threading.Tasks;

namespace OperatorConsole.Commands
{
    [Command("SetPort")]
    public class SetPortCommand : ClientCommand
    {
        public override string[] ArgTypes { get; protected set; } = { "port" };

#pragma warning disable
        protected override async Task<bool> Main()
        {
            if (argValues[ArgTypes[0]].Length != 1)
            {
                var message = new StringBuilder(); ;
                message.Append($"'SetPort' command can take just one 'port' ")
                       .Append($"| Count = {argValues[ArgTypes[0]].Length} :").AppendLine();

                foreach (var port in argValues[ArgTypes[0]])
                {
                    message.Append($"   //port = {port}").AppendLine();
                }

                flags.Message = message.ToString();
                return false;
            }

            try 
            {
                var url = new UriBuilder(Client.BaseUrl);
                url.Port = int.Parse(argValues[ArgTypes[0]][0]);
                Client.BaseUrl = url.ToString();
            }
            catch (Exception ex)
            {
                Logger?.Error("{@ex}", ex);
                flags.Message = "Error: " + ex.ToString();
                return false;
            }

            return true;
        }
    }
}
