﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Users
{
    [Command("UpdateUsers")]
    public class UpdateUsersCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutUserAsync(_id, Convert<Cleverence.User>(_object));
        protected sealed override Func<Task> onBeforeUpdate => null;
        protected sealed override Func<Task> onAfterUpdate => null;
    }
}
