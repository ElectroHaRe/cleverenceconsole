﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.UserGroups
{
    [Command("DeleteUserGroups")]
    public class DeleteUserGroupsCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteUserGroupAsync;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected sealed override Func<Task> onAfterDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetUserGroupsCommand();
    }
}
