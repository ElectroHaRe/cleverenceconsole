﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.UserGroups
{
    [Command("PostUserGroups")]
    public class PostUserGroupsCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostUserGroupAsync(Convert<Cleverence.UserGroup>(item));
        protected sealed override Func<Task> onBeforePost => null;
        protected sealed override Func<Task> onAfterPost => null;
    }
}
