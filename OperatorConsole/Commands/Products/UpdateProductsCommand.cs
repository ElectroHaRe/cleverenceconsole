﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Product
{
    [Command("UpdateProducts")]
    public class UpdateProductsCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutProductsAsync(_id, Convert<Cleverence.Product>(_object));
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateProductsAsync(null);
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateProductsAsync(null);
    }
}
