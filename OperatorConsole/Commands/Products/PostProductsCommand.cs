﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Product
{
    [Command("PostProducts")]
    public class PostProductsCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostProductsAsync(Convert<Cleverence.Product>(item));
        protected override Func<Task> onAfterPost => () => Client.EndUpdateProductsAsync(null);
        protected override Func<Task> onBeforePost => () => Client.BeginUpdateProductsAsync(null);
    }
}
