﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Product
{
    [Command("DeleteProducts")]
    public class DeleteProductsCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteProductsAsync;
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateProductsAsync(null);
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateProductsAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetProductsCommand();
    }
}
