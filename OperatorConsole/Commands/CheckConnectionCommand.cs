﻿using CommandSystem;
using OperatorConsole.Base;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OperatorConsole.Commands
{
    [Command("CheckConnection")]
    public class CheckConnectionCommand : ClientCommand
    {
        protected const string url = "url";

        public override string[] ArgTypes { get; protected set; } = { url };

#pragma warning disable
        protected override async Task<bool> Main()
        {
            if (!argValues.ContainsKey(url) || argValues[url].Length == 0)
                SetArgs($"{url} = {Client.BaseUrl}");

            foreach (var currentUrl in argValues[url])
            {
                HttpWebResponse response = null;

                try
                {
                    HttpWebRequest testRequest = (HttpWebRequest)HttpWebRequest.Create(currentUrl);
                    response = (HttpWebResponse)testRequest.GetResponse();
                    flags.Status = flags.Status.Append((int)response.StatusCode).ToArray();
                    flags.Result = flags.Result.Append(response.StatusCode == HttpStatusCode.OK).ToArray();
                }
                catch (WebException ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message = $"Error: {ex}";
                    flags.Status = flags.Status.Append((int)ex.Status).ToArray();
                    flags.Result = flags.Result.Append(false).ToArray();
                }
                finally { response?.Close(); }
            }

            return flags.Result.All((result) => result == true);
        }
    }
}
