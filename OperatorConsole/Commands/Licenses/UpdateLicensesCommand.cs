﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Licenses
{
    [Command("UpdateLicenses")]
    public class UpdateLicensesCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutLicensesAsync(_id, Convert<Cleverence.LicensingInfo>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
