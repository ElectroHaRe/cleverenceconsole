﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Licenses
{
    [Command("DeleteLicenses")]
    public class DeleteLicensesCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteLicensesAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetLicensesCommand();
    }
}
