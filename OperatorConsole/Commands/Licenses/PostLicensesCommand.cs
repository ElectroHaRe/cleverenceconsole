﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Licenses
{
    [Command("PostLicenses")]
    public class PostLicensesCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostLicensesAsync(Convert<Cleverence.LicensingInfo>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
