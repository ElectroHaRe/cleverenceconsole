﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Cells
{
    [Command("PostCells")]
    public class PostCellsCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostCellsAsync(Convert<Cleverence.Cell>(item));
        protected sealed override Func<Task> onBeforePost => null;
        protected sealed override Func<Task> onAfterPost => null;
    }
}
