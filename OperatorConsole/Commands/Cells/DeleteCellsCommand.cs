﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Cells
{
    [Command("DeleteCells")]
    public class DeleteCellsCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteCellsAsync;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected sealed override Func<Task> onAfterDelete => null;

        protected override Func<GetCommand> getCommand => () => new GetCellsCommand();
    }
}
