﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.SborShK
{
    [Command("DeleteSborShK")]
    public class DeleteSborShKCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteSborShKAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetSborShKCommand();
    }
}
