﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.SborShK
{
    [Command("UpdateSborShK")]
    public class UpdateSborShKCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutSborShKAsync(_id, Convert<Cleverence.SborShK>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
