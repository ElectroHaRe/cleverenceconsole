﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.SborShK
{
    [Command("PostSborShK")]
    public class PostSborShKCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostSborShKAsunc(Convert<Cleverence.SborShK>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
