﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PodborZakaza
{
    [Command("PostPodborZakaza")]
    public class PostPodborZakazaCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostPodborZakazaAsync(Convert<Cleverence.PodborZakaza>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
