﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PodborZakaza
{
    [Command("UpdatePodborZakaza")]
    public class UpdatePodborZakazaCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutPodborZakazaAsync(_id, Convert<Cleverence.PodborZakaza>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
