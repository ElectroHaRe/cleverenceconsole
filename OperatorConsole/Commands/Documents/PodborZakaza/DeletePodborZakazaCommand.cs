﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PodborZakaza
{
    [Command("DeletePodborZakaza")]
    public class DeletePodborZakazaCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeletePodborZakazaAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetPodborZakazaCommand();
    }
}
