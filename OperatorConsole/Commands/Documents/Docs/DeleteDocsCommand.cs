﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Docs
{
    [Command("DeleteDocs")]
    public class DeleteDocsCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteDocsAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetDocsCommand();
    }
}
