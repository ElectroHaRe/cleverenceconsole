﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Docs
{
    [Command("GetDocs")]
    public class GetDocsCommand : GetCommand
    {
        protected override sealed Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction =>
            async (_expand, _filter, _select, _orderBy, _top, _skip, _count) =>
            await Client.GetDocsAsync(_expand, _filter, _select, _orderBy, _top, _skip, _count);
    }
}
