﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Docs
{
    [Command("UpdateDocs")]
    public class UpdateDocsCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutDocsAsync(_id, Convert<Cleverence.Document>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
