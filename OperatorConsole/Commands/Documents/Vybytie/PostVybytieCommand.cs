﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Vybytie
{
    [Command("PostVybytie")]
    public class PostVybytieCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostVybytieAsync(Convert<Cleverence.Vybytie>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
