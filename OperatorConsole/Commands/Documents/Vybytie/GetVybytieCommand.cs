﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Vybytie
{
    [Command("GetVybytie")]
    public class GetVybytieCommand : GetCommand
    {
        protected override sealed Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction =>
            async (_expand, _filter, _select, _orderBy, _top, _skip, _count) =>
            await Client.GetVybytieAsync(_expand, _filter, _select, _orderBy, _top, _skip, _count);
    }
}
