﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Vybytie
{
    [Command("DeleteVybytie")]
    public class DeleteVybytieCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteVybytieAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetVybytieCommand();
    }
}
