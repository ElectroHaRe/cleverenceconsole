﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Inventarizaciya
{
    [Command("UpdateInventarizaciya")]
    public class UpdateInventarizaciyaCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutInventarizaciyaAsync(_id, Convert<Cleverence.Inventarizaciya>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
