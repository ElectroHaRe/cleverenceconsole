﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PeremeshheniePoYachejkam
{
    [Command("UpdatePeremeshheniePoYachejkam")]
    public class UpdatePeremeshheniePoYachejkamCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutPeremeshheniePoYachejkamAsync(_id, Convert<Cleverence.PeremeshheniePoYachejkam>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
