﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PeremeshheniePoYachejkam
{
    [Command("DeletePeremeshheniePoYachejkam")]
    public class DeletePeremeshheniePoYachejkamCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeletePeremeshheniePoYachejkamAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetPeremeshheniePoYachejkamCommand();
    }
}
