﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PeremeshheniePoYachejkam
{
    [Command("PostPeremeshheniePoYachejkam")]
    public class PostPeremeshheniePoYachejkamCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostPeremeshheniePoYachejkamAsync(Convert<Cleverence.PeremeshheniePoYachejkam>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
