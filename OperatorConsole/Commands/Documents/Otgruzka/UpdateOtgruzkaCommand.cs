﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.Otgruzka
{
    [Command("UpdateOtgruzka")]
    public class UpdateOtgruzkaCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutOtgruzkaAsync(_id, Convert<Cleverence.Otgruzka>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
