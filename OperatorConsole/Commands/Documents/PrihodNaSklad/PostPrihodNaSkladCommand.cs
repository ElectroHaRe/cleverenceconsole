﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PrihodNaSklad
{
    [Command("PostPrihodNaSklad")]
    public class PostPrihodNaSkladCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostPrihodNaSkladAsync(Convert<Cleverence.PrihodNaSklad>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
