﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PrihodNaSklad
{
    [Command("UpdatePrihodNaSklad")]
    public class UpdatePrihodNaSkladCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutPrihodNaSkladAsync(_id, Convert<Cleverence.PrihodNaSklad>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
