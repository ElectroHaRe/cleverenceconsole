﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PeremeshheniePoSkladam
{
    [Command("PostPeremeshheniePoSkladam")]
    public class PostPeremeshheniePoSkladamCommand : PostCommand
    {
        protected override sealed Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostPeremeshheniePoSkladamAsync(Convert<Cleverence.PeremeshheniePoSkladam>(item));
        protected override Func<Task> onAfterPost => null;
        protected override Func<Task> onBeforePost => null;
    }
}
