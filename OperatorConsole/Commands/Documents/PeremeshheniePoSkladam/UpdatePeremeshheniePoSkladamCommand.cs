﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PeremeshheniePoSkladam
{
    [Command("UpdatePeremeshheniePoSkladam")]
    public class UpdatePeremeshheniePoSkladamCommand : UpdateCommand
    {
        protected override sealed Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutPeremeshheniePoSkladamAsync(_id, Convert<Cleverence.PeremeshheniePoSkladam>(_object));
        protected override Func<Task> onAfterUpdate => null;
        protected override Func<Task> onBeforeUpdate => null;
    }
}
