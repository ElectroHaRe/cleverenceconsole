﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Documents.PeremeshheniePoSkladam
{
    [Command("DeletePeremeshheniePoSkladam")]
    public class DeletePeremeshheniePoSkladamCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeletePeremeshheniePoSkladamAsync;
        protected sealed override Func<Task> onAfterDelete => null;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetPeremeshheniePoSkladamCommand();
    }
}
