﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ceny
{
    [Command("DeleteCeny")]
    public class DeleteCenyCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteCenyAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateCenyAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateCenyAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetCenyCommand();
    }
}
