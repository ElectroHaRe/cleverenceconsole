﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ceny
{
    [Command("UpdateCeny")]
    public class UpdateCenyCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutCenyAsync(_id, Convert<Cleverence.Ceny>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateCenyAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateCenyAsync(null);
    }
}
