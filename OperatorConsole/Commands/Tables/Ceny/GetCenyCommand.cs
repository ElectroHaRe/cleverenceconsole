﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ceny
{
    [Command("GetCeny")]
    public class GetCenyCommand : GetCommand
    {
        protected override sealed Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction =>
            async (_expand, _filter, _select, _orderBy, _top, _skip, _count) =>
            await Client.GetCenyAsync(_expand, _filter, _select, _orderBy, _top, _skip, _count);
    }
}
