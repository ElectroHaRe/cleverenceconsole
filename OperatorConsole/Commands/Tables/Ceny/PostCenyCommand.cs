﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ceny
{
    [Command("PostCeny")]
    public class PostCenyCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostCenyAsync(Convert<Cleverence.Ceny>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateCenyAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateCenyAsync(null);
    }
}
