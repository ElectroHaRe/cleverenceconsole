﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.TransportnyeUpakovki
{
    [Command("PostTransportnyeUpakovki")]
    public class PostTransportnyeUpakovkiCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostTransportnyeUpakovkiAsync(Convert<Cleverence.TransportnyeUpakovki>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateTransportnyeUpakovkiAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateTransportnyeUpakovkiAsync(null);
    }
}
