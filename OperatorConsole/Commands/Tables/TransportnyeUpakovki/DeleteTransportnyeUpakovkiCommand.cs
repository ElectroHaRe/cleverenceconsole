﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.TransportnyeUpakovki
{
    [Command("DeleteTransportnyeUpakovki")]
    public class DeleteTransportnyeUpakovkiCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteTransportnyeUpakovkiAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateTransportnyeUpakovkiAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateTransportnyeUpakovkiAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetTransportnyeUpakovkiCommand();
    }
}
