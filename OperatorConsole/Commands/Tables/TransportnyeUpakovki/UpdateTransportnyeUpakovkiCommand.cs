﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.TransportnyeUpakovki
{
    [Command("UpdateTransportnyeUpakovki")]
    public class UpdateTransportnyeUpakovkiCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutTransportnyeUpakovkiAsync(_id, Convert<Cleverence.TransportnyeUpakovki>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateTransportnyeUpakovkiAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateTransportnyeUpakovkiAsync(null);
    }
}
