﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.DopRekvizity
{
    [Command("DeleteDopRekvizity")]
    public class DeleteDopRekvizityCommand: DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteDopRekvizityAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateDopRekvizityAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateDopRekvizityAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetDopRekvizityCommand();
    }
}
