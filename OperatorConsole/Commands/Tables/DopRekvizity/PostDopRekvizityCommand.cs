﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.DopRekvizity
{
    [Command("PostDopRekvizity")]
    public class PostDopRekvizityCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostDopRekvizityAsync(Convert<Cleverence.DopRekvizity>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateDopRekvizityAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateDopRekvizityAsync(null);
    }
}
