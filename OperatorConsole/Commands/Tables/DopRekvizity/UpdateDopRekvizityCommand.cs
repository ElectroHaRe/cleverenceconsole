﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.DopRekvizity
{
    [Command("UpdateDopRekvizity")]
    public class UpdateDopRekvizityCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutDopRekvizityAsync(_id, Convert<Cleverence.DopRekvizity>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateDopRekvizityAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateDopRekvizityAsync(null);
    }
}
