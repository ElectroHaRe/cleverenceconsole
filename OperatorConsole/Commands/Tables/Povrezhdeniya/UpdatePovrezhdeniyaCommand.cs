﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Povrezhdeniya
{
    [Command("UpdatePovrezhdeniya")]
    public class UpdatePovrezhdeniyaCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutPovrezhdeniyaAsync(_id, Convert<Cleverence.Povrezhdeniya>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdatePovrezhdeniyaAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdatePovrezhdeniyaAsync(null);
    }
}
