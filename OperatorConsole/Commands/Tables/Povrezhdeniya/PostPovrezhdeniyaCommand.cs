﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Povrezhdeniya
{
    [Command("PostPovrezhdeniya")]
    public class PostPovrezhdeniyaCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostPovrezhdeniyaAsync(Convert<Cleverence.Povrezhdeniya>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdatePovrezhdeniyaAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdatePovrezhdeniyaAsync(null);
    }
}
