﻿using CommandSystem;
using OperatorConsole.Base;
using OperatorConsole.Commands.Tables.Rezerv;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Povrezhdeniya
{
    [Command("DeletePovrezhdeniya")]
    public class DeleteRezervCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeletePovrezhdeniyaAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdatePovrezhdeniyaAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdatePovrezhdeniyaAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetRezervCommand();
    }
}
