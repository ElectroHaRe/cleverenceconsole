﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Uzly
{
    [Command("PostUzly")]
    public class PostUzlyCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostUzlyAsync(Convert<Cleverence.Uzly>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateUzlyAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateUzlyAsync(null);
    }
}
