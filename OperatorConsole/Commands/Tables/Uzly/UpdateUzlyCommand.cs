﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Uzly
{
    [Command("UpdateUzly")]
    public class UpdateUzlyCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutUzlyAsync(_id, Convert<Cleverence.Uzly>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateUzlyAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateUzlyAsync(null);
    }
}
