﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Uzly
{
    [Command("DeleteUzly")]
    public class DeleteUzlyCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteUzlyAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateUzlyAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateUzlyAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetUzlyCommand();
    }
}
