﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Uzly
{
    [Command("GetUzly")]
    public class GetUzlyCommand : GetCommand
    {
        protected sealed override Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction =>
            async (_expand, _filter, _select, _orderBy, _top, _skip, _count) =>
            await Client.GetUzlyAsync(_expand, _filter, _select, _orderBy, _top, _skip, _count);
    }
}
