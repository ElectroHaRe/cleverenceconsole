﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ostatki
{
    [Command("UpdateOstatki")]
    public class UpdateOstatkiCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutOstatkiAsync(_id, Convert<Cleverence.Ostatki>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateOstatkiAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateOstatkiAsync(null);
    }
}
