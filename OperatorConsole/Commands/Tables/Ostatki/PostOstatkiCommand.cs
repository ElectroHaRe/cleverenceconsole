﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ostatki
{
    [Command("PostOstatki")]
    public class PostOstatkiCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostOstatkiAsync(Convert<Cleverence.Ostatki>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateOstatkiAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateOstatkiAsync(null);
    }
}
