﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Ostatki
{
    [Command("DeleteOstatki")]
    public class DeleteOstatkiCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteOstatkiAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateOstatkiAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateOstatkiAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetOstatkiCommand();
    }
}
