﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Serii
{
    [Command("PostSerii")]
    public class PostSeriiCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostSeriiAsync(Convert<Cleverence.Serii>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateSeriiAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateSeriiAsync(null);
    }
}
