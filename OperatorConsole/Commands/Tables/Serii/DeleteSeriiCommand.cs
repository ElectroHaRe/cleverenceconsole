﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Serii
{
    [Command("DeleteSerii")]
    public class DeleteSeriiCommand: DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteSeriiAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateSeriiAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateSeriiAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetSeriiCommand();
    }
}
