﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Serii
{
    [Command("UpdateSerii")]
    public class UpdateSeriiCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutSeriiAsync(_id, Convert<Cleverence.Serii>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateSeriiAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateSeriiAsync(null);
    }
}
