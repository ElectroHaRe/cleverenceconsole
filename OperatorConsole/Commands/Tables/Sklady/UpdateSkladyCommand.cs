﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Sklady
{
    [Command("UpdateSklady")]
    public class UpdateSkladyCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutSkladyAsync(_id, Convert<Cleverence.Sklady>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateSkladyAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateSkladyAsync(null);
    }
}
