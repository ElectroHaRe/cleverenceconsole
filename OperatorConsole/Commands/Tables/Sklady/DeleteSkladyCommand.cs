﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Sklady
{
    [Command("DeleteSklady")]
    public class DeleteSkladyCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteSkladyAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateSkladyAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateSkladyAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetSkladyCommand();
    }
}
