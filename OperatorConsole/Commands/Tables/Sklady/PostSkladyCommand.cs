﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Sklady
{
    [Command("PostSklady")]
    public class PostSkladyCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostSkladyAsync(Convert<Cleverence.Sklady>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateSkladyAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateSkladyAsync(null);
    }
}
