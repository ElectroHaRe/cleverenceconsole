﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Rezerv
{
    [Command("UpdateRezerv")]
    public class UpdateRezervCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutRezervAsync(_id, Convert<Cleverence.Rezerv>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateRezervAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateRezervAsync(null);
    }
}
