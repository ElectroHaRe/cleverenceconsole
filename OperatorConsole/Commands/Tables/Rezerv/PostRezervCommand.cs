﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Rezerv
{
    [Command("PostRezerv")]
    public class PostRezervCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostRezervAsync(Convert<Cleverence.Rezerv>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateRezervAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateRezervAsync(null);
    }
}
