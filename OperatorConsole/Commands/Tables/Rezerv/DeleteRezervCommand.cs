﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Rezerv
{
    [Command("DeleteRezerv")]
    public class DeleteRezervCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeletePovrezhdeniyaAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateRezervAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateRezervAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetRezervCommand();
    }
}
