﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Kontragenty
{
    [Command("PostKontragenty")]
    public class PostKontragentyCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostKontragentyAsync(Convert<Cleverence.Kontragenty>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateKontragentyAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateKontragentyAsync(null);
    }
}
