﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Kontragenty
{
    [Command("UpdateKontragenty")]
    public class UpdateKontragentyCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutKontragentyAsync(_id, Convert<Cleverence.Kontragenty>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateKontragentyAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateKontragentyAsync(null);
    }
}
