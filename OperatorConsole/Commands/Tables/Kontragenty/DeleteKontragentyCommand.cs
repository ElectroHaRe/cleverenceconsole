﻿using CommandSystem;
using OperatorConsole.Base;
using OperatorConsole.Commands.Tables.Rezerv;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Kontragenty
{
    [Command("DeleteKontragenty")]
    public class DeleteKontragentyCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteKontragentyAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateKontragentyAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateKontragentyAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetKontragentyCommand();
    }
}
