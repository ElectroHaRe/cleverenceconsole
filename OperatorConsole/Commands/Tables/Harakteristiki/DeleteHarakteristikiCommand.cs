﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Harakteristiki
{
    [Command("DeleteHarakteristiki")]
    public class DeleteHarakteristikiCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteHarakteristikiAsync;
        protected sealed override Func<Task> onBeforeDelete => () => Client.BeginUpdateHarakteristikiAsync(null);
        protected sealed override Func<Task> onAfterDelete => () => Client.EndUpdateHarakteristikiAsync(null);
        protected override Func<GetCommand> getCommand => () => new GetHarakteristikiCommand();
    }
}
