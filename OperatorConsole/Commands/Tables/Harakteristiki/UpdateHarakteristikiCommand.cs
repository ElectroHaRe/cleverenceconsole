﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Harakteristiki
{
    [Command("UpdateHarakteristiki")]
    public class UpdateHarakteristikiCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutHarakteristikiAsync(_id, Convert<Cleverence.Harakteristiki>(_object));
        protected sealed override Func<Task> onBeforeUpdate => () => Client.BeginUpdateHarakteristikiAsync(null);
        protected sealed override Func<Task> onAfterUpdate => () => Client.EndUpdateHarakteristikiAsync(null);
    }
}
