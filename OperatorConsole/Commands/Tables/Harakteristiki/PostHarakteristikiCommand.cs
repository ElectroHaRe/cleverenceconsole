﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Tables.Harakteristiki
{
    [Command("PostHarakteristiki")]
    public class PostHarakteristikiCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostHarakteristikiAsync(Convert<Cleverence.Harakteristiki>(item));
        protected sealed override Func<Task> onBeforePost => () => Client.BeginUpdateHarakteristikiAsync(null);
        protected sealed override Func<Task> onAfterPost => () => Client.EndUpdateHarakteristikiAsync(null);
    }
}
