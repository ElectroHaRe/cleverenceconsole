﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.CustomSettings
{
    [Command("GetCustomSettings")]
    public class GetCustomSettingsCommand : GetCommand
    {
        protected sealed override Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction =>
            async (_expand, _filter, _select, _orderBy, _top, _skip, _count) =>
            await Client.GetCustomSettingsAsync(_expand, _filter, _select, _orderBy, _top, _skip, _count);
    }
}
