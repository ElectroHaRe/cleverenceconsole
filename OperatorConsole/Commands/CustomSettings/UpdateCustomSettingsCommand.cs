﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.CustomSettings
{
    [Command("UpdateCustomSettings")]
    public class UpdateCustomSettingsCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutCustomSettingsAsync(_id, Convert<object>(_object));
        protected sealed override Func<Task> onBeforeUpdate => null;
        protected sealed override Func<Task> onAfterUpdate => null;
    }
}
