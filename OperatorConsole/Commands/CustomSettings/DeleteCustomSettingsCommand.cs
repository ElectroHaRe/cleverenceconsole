﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.CustomSettings
{
    [Command("DeleteCustomSettings")]
    public class DeleteCustomSettingsCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteCustomSettingsAsync;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected sealed override Func<Task> onAfterDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetCustomSettingsCommand();
    }
}
