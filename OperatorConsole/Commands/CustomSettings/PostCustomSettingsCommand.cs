﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.CustomSettings
{
    [Command("PostCustomSettings")]
    public class PostCustomSettingsCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => { await Client.PostCustomSettingsAsync(Convert<object>(item)); return null; };
        protected sealed override Func<Task> onBeforePost => null;
        protected sealed override Func<Task> onAfterPost => null;
    }
}
