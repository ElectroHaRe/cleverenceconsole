﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.DeviceInfo
{
    [Command("UpdateDevices")]
    public class UpdateDevicesCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutDeviceInfoAsync(_id, Convert<Cleverence.DeviceInfo>(_object));
        protected sealed override Func<Task> onBeforeUpdate => null;
        protected sealed override Func<Task> onAfterUpdate => null;
    }
}
