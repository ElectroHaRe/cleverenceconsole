﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.DeviceInfo
{
    [Command("PostDevices")]
    public class PostDevicesCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostDeviceInfoAsync(Convert<Cleverence.DeviceInfo>(item));
        protected sealed override Func<Task> onBeforePost => null;
        protected sealed override Func<Task> onAfterPost => null;
    }
}
