﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.DeviceInfo
{
    [Command("GetDevices")]
    public class GetDevicesCommand : GetCommand
    {
        protected sealed override Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction =>
            async (_expand, _filter, _select, _orderBy, _top, _skip, _count) =>
            await Client.GetDeviceInfoAsync(_expand, _filter, _select, _orderBy, _top, _skip, _count);
    }
}
