﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Warehouses
{
    [Command("DeleteWarehouses")]
    public class DeleteWarehousesCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteWarehouseAsync;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected sealed override Func<Task> onAfterDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetWarehousesCommand();
    }
}
