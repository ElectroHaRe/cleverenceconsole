﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.Warehouses
{
    [Command("UpdateWarehouses")]
    public class UpdateWarehousesCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutWarehouseAsync(_id, Convert<Cleverence.Warehouse>(_object));
        protected sealed override Func<Task> onBeforeUpdate => null;
        protected sealed override Func<Task> onAfterUpdate => null;
    }
}
