﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.TablesInfo
{
    [Command("UpdateTablesInfo")]
    public class UpdateTablesInfoCommand : UpdateCommand
    {
        protected sealed override Func<string, object, Task> updateFunction =>
            async (_id, _object) => await Client.PutDocumentTableInfoAsync(_id, Convert<Cleverence.DocumentTableInfo>(_object));
        protected sealed override Func<Task> onBeforeUpdate => null;
        protected sealed override Func<Task> onAfterUpdate => null;
    }
}
