﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.TablesInfo
{
    [Command("PostTablesInfo")]
    public class PostTablesInfoCommand : PostCommand
    {
        protected sealed override Func<object, Task<object>> postFunction =>
            async (item) => await Client.PostDocumentTableInfoAsync(Convert<Cleverence.DocumentTableInfo>(item));
        protected sealed override Func<Task> onBeforePost => null;
        protected sealed override Func<Task> onAfterPost => null;
    }
}
