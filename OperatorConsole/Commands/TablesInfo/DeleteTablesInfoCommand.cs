﻿using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Threading.Tasks;

namespace OperatorConsole.Commands.TablesInfo
{
    [Command("DeleteTablesInfo")]
    public class DeleteTablesInfoCommand : DeleteCommand
    {
        protected sealed override Func<string, string, Task> deleteByIdFunction => Client.DeleteDocumentTableInfoAsync;
        protected sealed override Func<Task> onBeforeDelete => null;
        protected sealed override Func<Task> onAfterDelete => null;
        protected override Func<GetCommand> getCommand => () => new GetTablesInfoCommand();
    }
}
