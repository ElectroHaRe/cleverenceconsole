﻿using Cleverence;
using CommandSystem;
using OperatorConsole.Base;
using System;
using System.Text;
using System.Threading.Tasks;

namespace OperatorConsole.Commands
{
    [Command("SetUrl")]
    public class SetUrlCommand : ClientCommand
    {
        public override string[] ArgTypes { get; protected set; } = { "url" };

#pragma warning disable
        protected override async Task<bool> Main()
        {
            if (argValues[ArgTypes[0]].Length != 1)
            {
                var message = new StringBuilder(); ;
                message.Append($"'SetUrl' command can take just one 'url' ")
                       .Append($"| Count = {argValues[ArgTypes[0]].Length} :").AppendLine();

                foreach (var url in argValues[ArgTypes[0]])
                {
                    message.Append($"   //Url = {url}").AppendLine();
                }

                flags.Message = message.ToString();
                return false;
            }

            try { Client.BaseUrl = argValues[ArgTypes[0]][0]; }
            catch (Exception ex)
            {
                Logger?.Error("{@ex}", ex);
                flags.Message = "Error: " + ex.ToString();
                return false;
            }

            return true;
        }
    }
}
