﻿using Cleverence;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OperatorConsole.Base
{
    public abstract class UpdateCommand : ClientCommand
    {
        protected const string key = "key";
        protected const string path = "path";

        public sealed override string[] ArgTypes { get; protected set; } = { key, path };

        protected abstract Func<string, object, Task> updateFunction { get; }
        protected abstract Func<Task> onBeforeUpdate { get; }
        protected abstract Func<Task> onAfterUpdate { get; }

        protected override async sealed Task<bool> Main()
        {
            object[] objects = new object[0];
            foreach (var path in argValues[path])
            {
                try { objects = objects.Concat(GetObject<object[]>(path)).ToArray(); }
                catch (JsonException)
                { objects = objects.Append(GetObject<object>(path)).ToArray(); }
                catch (Exception ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += "Error: " + ex.ToString();
                    return false;
                }
            }

            if (objects.Length != argValues[key].Length)
            {
                flags.Message += $"Error: The number of devices and ids must be the same. " +
                    $"| device count = {objects.Length}, " +
                    $"but id count = {argValues[key].Length}";
                return false;
            }

            if (onBeforeUpdate != null)
                await onBeforeUpdate?.Invoke();

            for (int i = 0; i < objects.Length; i++)
            {
                try
                {
                    await updateFunction(argValues[key][i], objects[i]);
                    flags.Result = flags.Result.Append(true).ToArray();
                    flags.Status = flags.Status.Append(200).ToArray();
                }
                catch (ApiException ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += $"Device id = {argValues[key][i]} | Status Code = {ex.StatusCode} | Error: {ex}\n";
                    flags.Result = flags.Result.Append(false).ToArray();
                    flags.Status = flags.Status.Append(ex.StatusCode).ToArray();
                }
                catch (Exception ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += $"Device id = {argValues[key][i]} | Error: {ex}\n";
                    flags.Result = flags.Result.Append(false).ToArray();
                    flags.Status = flags.Status.Append(-1).ToArray();
                }
            }

            if (onAfterUpdate != null)
                await onAfterUpdate?.Invoke();

            return flags.Result.All((result) => result == true);
        }
    }
}
