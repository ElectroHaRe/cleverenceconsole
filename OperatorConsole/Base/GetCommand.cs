﻿using Cleverence;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OperatorConsole.Base
{
    public abstract class GetCommand : ClientCommand
    {
        protected const string filter = "filter";
        protected const string select = "select";
        protected const string orderBy = "order_by";
        protected const string top = "top";
        protected const string skip = "skip";
        protected const string count = "count";

        public sealed override string[] ArgTypes { get; protected set; } = { filter, select, orderBy, top, skip, count };

        protected abstract Func<string, string, string, string, int?, int?, bool?, Task<object>> getFunction { get; }

        protected override async sealed Task<bool> Main()
        {
            object responseResult = null;

            try
            {
                try
                {
                    responseResult = await getFunction
                         ("declaredItems,currentItems,combinedItems,tables($expand=rows)",
                         argValues.ContainsKey(filter) && argValues[filter].Length > 0 ? string.Concat(argValues[filter][0]) : null,
                         argValues.ContainsKey(select) && argValues[select].Length > 0 ? string.Concat(argValues[select][0]) : null,
                         argValues.ContainsKey(orderBy) && argValues[orderBy].Length > 0 ? string.Concat(argValues[orderBy][0]) : null,
                         argValues.ContainsKey(top) && argValues[top].Length > 0 ? int.Parse(argValues[top][0]) : (int?)null,
                         argValues.ContainsKey(skip) && argValues[skip].Length > 0 ? int.Parse(argValues[skip][0]) : (int?)null,
                         argValues.ContainsKey(count) && argValues[count].Length > 0 ? bool.Parse(argValues[count][0]) : (bool?)null);
                }
                catch (ApiException)
                {
                    responseResult = await getFunction
                         (null,
                         argValues.ContainsKey(filter) && argValues[filter].Length > 0 ? string.Concat(argValues[filter][0]) : null,
                         argValues.ContainsKey(select) && argValues[select].Length > 0 ? string.Concat(argValues[select][0]) : null,
                         argValues.ContainsKey(orderBy) && argValues[orderBy].Length > 0 ? string.Concat(argValues[orderBy][0]) : null,
                         argValues.ContainsKey(top) && argValues[top].Length > 0 ? int.Parse(argValues[top][0]) : (int?)null,
                         argValues.ContainsKey(skip) && argValues[skip].Length > 0 ? int.Parse(argValues[skip][0]) : (int?)null,
                         argValues.ContainsKey(count) && argValues[count].Length > 0 ? bool.Parse(argValues[count][0]) : (bool?)null);
                }

                flags.Status = flags.Status.Append(200).ToArray();
                flags.Objects = (responseResult as dynamic).Value.ToArray();
                flags.Result = flags.Result.Append(true).ToArray();
            }
            catch (ApiException ex)
            {
                Logger?.Error("{@ex}", ex);
                flags.Message = $"Status Code = {ex.StatusCode} | Error: {ex}";
                flags.Status = flags.Status.Append(ex.StatusCode).ToArray();
                flags.Result = flags.Result.Append(false).ToArray();
            }
            catch (Exception ex)
            {
                Logger?.Error("{@ex}", ex);
                flags.Message = $"Error: {ex}";
                flags.Status = flags.Status.Append(-1).ToArray();
                flags.Result = flags.Result.Append(false).ToArray();
            }

            return flags.Result.All((result) => result == true);
        }
    }
}
