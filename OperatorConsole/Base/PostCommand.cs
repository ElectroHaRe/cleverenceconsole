﻿using Cleverence;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OperatorConsole.Base
{
    public abstract class PostCommand : ClientCommand
    {
        protected const string path = "path";

        public sealed override string[] ArgTypes { get; protected set; } = { path };

        protected abstract Func<object, Task<object>> postFunction { get; }
        protected abstract Func<Task> onBeforePost { get; }
        protected abstract Func<Task> onAfterPost { get; }

        protected override async sealed Task<bool> Main()
        {
            object[] objects = new object[0];
            foreach (var path in argValues[path])
            {
                try
                {
                    try { objects = objects.Concat(GetObject<object[]>(path)).ToArray(); }
                    catch (JsonException ex)
                    {
                        objects = objects.Append(GetObject<JsonObject>(path).value ?? throw ex).ToArray();
                    }
                }
                catch (JsonException)
                { objects = objects.Append(GetObject<object>(path)).ToArray(); }
                catch (Exception ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += "Error: " + ex.ToString();
                    return false;
                }
            }

            if (onBeforePost != null)
                await onBeforePost?.Invoke();

            foreach (var obj in objects)
            {
                try
                {
                    await postFunction(obj);
                    flags.Result = flags.Result.Append(true).ToArray();
                    flags.Status = flags.Status.Append(200).ToArray();
                }
                catch (ApiException ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += $"Status Code = {ex.StatusCode} | Error: {ex}\n";
                    flags.Result = flags.Result.Append(false).ToArray();
                    flags.Status = flags.Status.Append(ex.StatusCode).ToArray();
                }
                catch (Exception ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += $"Error: {ex}\n";
                    flags.Result = flags.Result.Append(false).ToArray();
                    flags.Status = flags.Status.Append(-1).ToArray();
                }
            }

            if (onAfterPost != null)
                await onAfterPost?.Invoke();

            return flags.Result.All((result) => result == true);
        }
    }
}
