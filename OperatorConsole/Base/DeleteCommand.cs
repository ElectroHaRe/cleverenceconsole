﻿using Cleverence;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OperatorConsole.Base
{
    public abstract class DeleteCommand : ClientCommand
    {
        protected const string key = "key";
        protected const string keyExpression = "key_expr";

        public sealed override string[] ArgTypes { get; protected set; } = { key, keyExpression };

        protected abstract Func<Task> onBeforeDelete { get; }
        protected abstract Func<Task> onAfterDelete { get; }
        protected abstract Func<string, string, Task> deleteByIdFunction { get; }
        protected abstract Func<GetCommand> getCommand { get; }

        protected async override sealed Task<bool> Main()
        {
            if (argValues[keyExpression].Length > 0 && getCommand != null)
            {
                string[] expressions = new string[0];
                var getCommand = this.getCommand?.Invoke();
                getCommand.SetArgs(); getCommand.Logger = Logger; getCommand.Client = Client;
                foreach (var expr in argValues[keyExpression])
                {
                    expressions = expressions.Append(expr).ToArray();
                }
                getCommand.SetCheckList(expressions.ToArray());
                await getCommand.Run();
                flags = getCommand.flags;
                foreach (IEnumerator<JToken> obj in getCommand.flags.Objects)
                {
                    argValues[key] = argValues[key].Append(obj.Current.ToString()).ToArray();
                }
            }

            if (!argValues.ContainsKey(key))
                return true;

            if (onBeforeDelete != null)
                await onBeforeDelete?.Invoke();

            foreach (var id in argValues[key])
            {
                try
                {
                    await deleteByIdFunction(id, string.Empty);
                    flags.Result = flags.Result.Append(true).ToArray();
                    flags.Status = flags.Status.Append(200).ToArray();
                }
                catch (ApiException ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += $"id = {id} | Status Code = {ex.StatusCode} | Error: {ex}\n";
                    flags.Result = flags.Result.Append(false).ToArray();
                    flags.Status = flags.Status.Append(ex.StatusCode).ToArray();
                }
                catch (Exception ex)
                {
                    Logger?.Error("{@ex}", ex);
                    flags.Message += $"Error: {ex}\n";
                    flags.Result = flags.Result.Append(false).ToArray();
                    flags.Status = flags.Status.Append(-1).ToArray();
                }
            }
            if (onAfterDelete != null)
                await onAfterDelete?.Invoke();

            return flags.Result.All((result) => result == true);
        }
    }
}
