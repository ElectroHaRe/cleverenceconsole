﻿using Cleverence;
using CommandSystem;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace OperatorConsole.Base
{
    public class ClientCommandHandler : CommandHandler
    {
        public const string LoadScriptOperator = "load_script";
        public const string CommandOperator = "command";
        public const string ArgsOperator = "args";
        public const string CheckOperator = "check";
        public const string HightPriorityOperator = "hight_priority";
        public const string WaitOperator = "wait";
        public const string IntervalOperator = "interval";
        public const char OperatorSeparator = ':';

        protected static readonly string[] Operators =
            { LoadScriptOperator, CommandOperator, ArgsOperator, CheckOperator, HightPriorityOperator, WaitOperator, IntervalOperator };

        public Action<string> OnReportUpdated;

        public readonly Client Client;
        public readonly ILogger Logger;

        public string Report => reportBuilder.ToString();

        private StringBuilder reportBuilder = new StringBuilder();

        public ClientCommandHandler(Client client, params string[] input)
        {
            OnCommandStart += CommandStartHandler;
            OnCommandFinish += CommandFinishHandler;
            Client = client;
            InitializeCommands(input);
        }

        public ClientCommandHandler(Client client, ILogger logger, params string[] input)
        {
            OnCommandStart += CommandStartHandler;
            OnCommandFinish += CommandFinishHandler;
            Client = client;
            Logger = logger;
            InitializeCommands(input);
        }

        public void ClearReport() => reportBuilder.Clear();

        protected override KeyValuePair<string, string[]>[] GetNameValuesArray(params string[] input)
        {
            if (input == null)
                return new KeyValuePair<string, string[]>[0];

            var result = new KeyValuePair<string, string[]>[0];
            var operators = DistributeOperatorsByLines(input);

            var currentCommand = string.Empty;
            var values = new string[0];

            foreach (var currentOperator in operators)
            {
                if (currentOperator.Contains(CommandOperator + OperatorSeparator))
                {
                    SaveLastCommand();
                    currentCommand = currentOperator.Replace(CommandOperator + OperatorSeparator, string.Empty).Trim(' ');
                    continue;
                }

                if (currentOperator.Contains(LoadScriptOperator + OperatorSeparator))
                {
                    SaveLastCommand();
                    result = result.Concat(
                        GetNameValuesArrayFromFile(currentOperator.Replace
                        (LoadScriptOperator + OperatorSeparator, string.Empty).Trim(' '))).ToArray();
                    continue;
                }

                if (currentOperator.Contains(ArgsOperator + OperatorSeparator)
                    || currentOperator.Contains(CheckOperator + OperatorSeparator)
                    || currentOperator.Contains(HightPriorityOperator + OperatorSeparator)
                    || currentOperator.Contains(WaitOperator + OperatorSeparator)
                    || currentOperator.Contains(IntervalOperator + OperatorSeparator))
                {
                    if (string.IsNullOrEmpty(currentCommand))
                        throw new ArgumentException($"Error: No parent arguments detected | Value: {currentOperator}");

                    values = values.Append(currentOperator).ToArray();
                    continue;
                }
            }

            SaveLastCommand();

            return result;

            void SaveLastCommand()
            {
                if (string.IsNullOrEmpty(currentCommand))
                    return;
                result = result.Append(new KeyValuePair<string, string[]>(currentCommand, values)).ToArray();
                currentCommand = string.Empty;
                values = new string[0];
            }
        }

        protected override bool CommandSelector(Command command)
        {
            return base.CommandSelector(command) && command is ClientCommand;
        }

        protected override void SetupCommand(Command command, params string[] args)
        {
            (command as ClientCommand).Client = Client;
            (command as ClientCommand).Logger = Logger;

            var argsList = new string[0];
            var checkList = new string[0];
            var hightPriority = false;
            double? waitTime = null;
            double? intervalTime = null;

            foreach (var arg in args)
            {
                if (arg.Contains(ArgsOperator + OperatorSeparator))
                    argsList = argsList.Append(arg.Replace(ArgsOperator + OperatorSeparator, string.Empty).Trim(' ')).ToArray();
                else if (arg.Contains(CheckOperator + OperatorSeparator))
                    checkList = checkList.Append(arg.Replace(CheckOperator + OperatorSeparator, string.Empty).Trim(' ')).ToArray();
                else if (arg.Contains(HightPriorityOperator + OperatorSeparator))
                    hightPriority = true;
                else if (arg.Contains(WaitOperator + OperatorSeparator))
                    waitTime = double.Parse(arg.Replace(WaitOperator + OperatorSeparator, string.Empty));
                else if (arg.Contains(IntervalOperator + OperatorSeparator))
                    intervalTime = double.Parse(arg.Replace(IntervalOperator + OperatorSeparator, string.Empty));
                else throw new ArgumentException($"Error: Unrecognized type of command arguments " +
                                                 $"| {CommandOperator}{OperatorSeparator} " +
                                                 $"{command.GetType().GetCustomAttribute<CommandAttribute>().Name}" +
                                                 $"| {ArgsOperator}{OperatorSeparator} {arg}");
            }

            if (checkList.Length > 0 && !(command is ClientCommand))
                throw new ArgumentException($"An attempt was made to create a checklist for an unsupported command " +
                                            $"'{command.GetType().GetCustomAttribute<CommandAttribute>().Name}'");

            if (hightPriority && !(command is ClientCommand))
                throw new ArgumentException($"An attempt was made to set a high priority for an unsupported command " +
                                            $"'{command.GetType().GetCustomAttribute<CommandAttribute>().Name}'");

            if (hightPriority && !(command is ClientCommand))
                throw new ArgumentException($"An attempt was made to set a waiting time for an unsupported command " +
                                            $"'{command.GetType().GetCustomAttribute<CommandAttribute>().Name}'");

            if (hightPriority && !(command is ClientCommand))
                throw new ArgumentException($"An attempt was made to set an interval time for an unsupported command " +
                                            $"'{command.GetType().GetCustomAttribute<CommandAttribute>().Name}'");

            if (argsList.Length > 0)
                command.SetArgs(argsList);

            if (checkList.Length > 0)
                (command as ClientCommand).SetCheckList(checkList);

            if (hightPriority)
                (command as ClientCommand).SetHightPriority();

            if (waitTime != null)
                (command as ClientCommand).WaitingTime = waitTime;

            if (intervalTime != null)
                (command as ClientCommand).IntervalTime = intervalTime;
        }

        private KeyValuePair<string, string[]>[] GetNameValuesArrayFromFile(string path)
        {
            path = path.Replace(LoadScriptOperator, string.Empty).TrimStart(' ');
            if (!Path.IsPathRooted(path))
                path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), path);
            var commandString = File.ReadAllText(path);
            return GetNameValuesArray(commandString);
        }

        private void CommandStartHandler(string name, Command command)
        {
            reportBuilder.AppendLine((command as ClientCommand)?.flags.HightPriority ?? false ?
                $"command: {name} ({HightPriorityOperator})" : $"command: {name}");

            if (command is ClientCommand)
            {
                if ((command as ClientCommand).WaitingTime != null)
                {
                    reportBuilder.AppendLine($"wait: {(command as ClientCommand).WaitingTime.Value}");

                    if ((command as ClientCommand).IntervalTime != null)
                        reportBuilder.AppendLine($"interval: {(command as ClientCommand).IntervalTime.Value}");
                }

                if ((command as ClientCommand).CheckList.Length > 0)
                {
                    reportBuilder.AppendLine("check: ");
                    foreach (var checker in (command as ClientCommand).CheckList)
                    {
                        reportBuilder.AppendLine($@"        {checker}");
                    }
                }

            }

            var args = command.GetArgs();
            if (args.Length > 0)
            {
                reportBuilder.AppendLine($"args: ");
                foreach (var arg in command.GetArgs())
                {
                    reportBuilder.AppendLine($@"        {arg}");
                }
            }

            OnReportUpdated?.Invoke(Report);
        }

        private void CommandFinishHandler(string name, Command command, bool result)
        {
            if (!string.IsNullOrEmpty((command as ClientCommand)?.flags.Message))
                reportBuilder.AppendLine($"message: {(command as ClientCommand).flags.Message}");
            if ((command as ClientCommand)?.flags.Objects.Length > 0)
                reportBuilder.AppendLine($"objects: " +
                    $"\n{JsonConvert.SerializeObject((command as ClientCommand)?.flags.Objects, Formatting.Indented)}");
            reportBuilder.AppendLine($"result: {result}");
            reportBuilder.AppendLine("--------------------");

            OnReportUpdated?.Invoke(Report);

            if (!result && ((command as ClientCommand)?.flags.HightPriority ?? false))
                Environment.Exit(1);
        }

        private string[] DistributeOperatorsByLines(params string[] operators)
        {
            string[] result = new string[0];

            string temp = string.Empty;
            foreach (var item in operators)
            {
                temp += ' ' + item;
            }
            temp = Regex.Replace(temp, @"\s+", " ").Trim(' ');
            foreach (var item in Operators)
            {
                temp = Regex.Replace(temp, $@"{item}\s*{OperatorSeparator}", "\n" + item + OperatorSeparator);
            }

            foreach (var item in Regex.Matches(temp, "[^\n]+"))
            {
                result = result.Append(item.ToString().Trim(' ')).ToArray();
            }
            return result;
        }
    }
}