﻿using Cleverence;
using CommandSystem;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OperatorConsole.Base
{
    public abstract class ClientCommand : Command
    {
        public abstract string[] ArgTypes { get; protected set; }

        public string[] CheckList = new string[0];

        public double? WaitingTime = null; //min
        public double? IntervalTime = null; //sec

        public class Flags
        {
            public bool HightPriority = false;
            public string Message = string.Empty;
            public int[] Status = new int[0];
            public bool[] Result = new bool[0];
            public int Count => Objects?.Length ?? 0;
            public object[] Objects = new object[0];
        }

        public Flags flags { get; set; } = new Flags();

        public Client Client;
        public ILogger Logger;

        protected class JsonObject
        {
            public object[] value;
        }

        protected Dictionary<string, string[]> argValues = new Dictionary<string, string[]>();

        public readonly Dictionary<string, string> Results = new Dictionary<string, string>();

        public ClientCommand(Client client = null, ILogger logger = null)
        {
            Client = client;
            Logger = logger;

            foreach (var argType in ArgTypes)
            {
                if (!argValues.ContainsKey(argType))
                    argValues.Add(argType, new string[0]);
            }
        }

        public sealed override async Task<bool> Run()
        {
            var startTime = DateTime.Now;

            bool result = await Main();

            if (CheckList.Length > 0)
                result = Check(flags, CheckList);

            if (!WaitingTime.HasValue || result)
                return result;

            var destinationTime = DateTime.Now.AddMinutes(WaitingTime.Value) - (DateTime.Now - startTime);
            var intervalTime = TimeSpan.FromSeconds(IntervalTime.HasValue ? IntervalTime.Value : WaitingTime.Value * 60 / 100);

            while (DateTime.Now < destinationTime && !result)
            {
                Thread.Sleep(intervalTime);
                flags = new Flags() { HightPriority = flags.HightPriority };
                result = await Main();
                if (CheckList.Length > 0)
                    result = Check(flags, CheckList);
            }

            return result;
        }

        public void SetHightPriority()
        {
            flags.HightPriority = true;
        }

        public virtual void SetCheckList(params string[] checkList)
        {
            if (checkList == null || checkList.Length == 0)
                return;

            foreach (var checker in checkList)
            {
                CheckList = CheckList.Append(checker).ToArray();
            }
        }

        public override void SetArgs(params string[] args)
        {
            string source = string.Empty;
            foreach (var line in args)
            {
                source += " " + line;
            }
            source.Trim(' ');

            foreach (var argType in ArgTypes)
            {
                source = Regex.Replace(source, $@"{argType}\s?=", "\n" + $"{argType}=");
            }

            foreach (var argType in ArgTypes)
            {
                if (!argValues.ContainsKey(argType))
                    argValues.Add(argType, new string[0]);

                string expr = $@"((?<={argType}(\s?)=).+)";

                foreach (var value in Regex.Matches(source, expr))
                {
                    argValues[argType] = argValues[argType].Append(value.ToString().Trim(' ')).ToArray();
                    Args.Add(argType + " = " + argValues[argType].Last());
                }
            }
        }

        protected abstract Task<bool> Main();

        Dictionary<string, bool?> checkResults = new Dictionary<string, bool?>();
        protected virtual bool Check(Flags flags, params string[] checkList)
        {
            var objects = new object[0];
            var json = JObject.FromObject(flags);
            bool currentResult = false;
            foreach (var expr in checkList)
            {
                if (!checkResults.ContainsKey(expr))
                    checkResults.Add(expr, null);

                var nodes = json.SelectTokens(expr);
                currentResult = nodes.Count() != 0;

                if (currentResult != checkResults[expr])
                {
                    checkResults[expr] = currentResult;
                    Logger?.Information($"check: {expr} | result: {currentResult}");
                }

                if (currentResult == false)
                    return false;

                objects = objects.Append(nodes).ToArray();
            }
            flags.Objects = objects;
            return true;
        }

        protected string GetCorrectUid(string uidFromServer) =>
            Regex.Match(uidFromServer, @"[0-9]+$").ToString();

        protected string GetCorrectPath(string path)
        {
            path.Trim(' ');
            if (!Path.IsPathRooted(path))
                path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), path);
            return path;
        }

        protected T GetObject<T>(string inputJson, bool isPath = true)
        {
            string json = isPath ? File.ReadAllText(GetCorrectPath(inputJson)) : inputJson;
            return JsonConvert.DeserializeObject<T>(json);
        }

        protected T Convert<T>(object obj)
        {
            return (T)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(obj), typeof(T));
        }
    }
}