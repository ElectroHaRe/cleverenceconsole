﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CommandSystem
{
    public class CommandHandler
    {
        /// <summary>
        /// Pass this constant, if you want to find all the commands matching the selectors
        /// </summary>
        public const string All = "_All_";
        private const string quote = "'";

        protected Queue<KeyValuePair<string, Command>> commands = new Queue<KeyValuePair<string, Command>>();

        public static string NameExpr { get; private set; } = @"^[A-z_]+[0-9A-z_]*";
        public static string ValueExpr { get; private set; } =
            $@"([{quote}]" + @"{1}" + $@"[^{quote}]*[{quote}]" + @"{1})|([0-9A-z_]+[0-9A-z_]*)";

        public event Action<string, Command> OnCommandStart;
        public event Action<string, Command, bool> OnCommandFinish;

        protected CommandHandler() { }

        public CommandHandler(params string[] input)
        {
            InitializeCommands(input);
        }

        public async Task<KeyValuePair<string, bool>[]> RunCommands()
        {
            var commandResults = new KeyValuePair<string, bool>[commands.Count];
            int i = 0;
            while (commands.Count > 0)
            {
                i = commandResults.Length - commands.Count;
                var command = commands.Dequeue();
                OnCommandStart?.Invoke(command.Key, command.Value);
                commandResults[i] = new KeyValuePair<string, bool>(command.Key, await command.Value.Run());
                OnCommandFinish?.Invoke(command.Key, command.Value, commandResults[i].Value);
            }
            return commandResults;
        }

        protected void InitializeCommands(params string[] input)
        {
            commands.Clear();
            var temp = GetNameValuesArray(input);
            FindCommands(temp);
        }

        protected virtual void SetupCommand(Command command, params string[] args)
        {
            command.SetArgs(args);
        }

        protected virtual bool TypeSelector(Type commandType)
        {
            try { return Activator.CreateInstance(commandType) is Command; }
            catch { return false; }
        }

        protected virtual bool AttributeSelector(CommandAttribute attribute)
        {
            return attribute != null;
        }

        protected virtual bool CommandSelector(Command command)
        {
            return command != null;
        }

        protected virtual KeyValuePair<string, string[]>[] GetNameValuesArray(params string[] input)
        {
            if (input == null)
                return new KeyValuePair<string, string[]>[0];

            var nameValuesArray = new KeyValuePair<string, string[]>[0];

            var nameRgx = new Regex(NameExpr);
            var valueRgx = new Regex(ValueExpr);

            string name, valueStr;
            foreach (var str in input)
            {
                name = nameRgx.Match(str)?.ToString();

                if (string.IsNullOrEmpty(str))
                    continue;

                valueStr = nameRgx.Replace(str, string.Empty);
                var args = valueRgx.Matches(valueStr);

                Array.Resize(ref nameValuesArray, nameValuesArray.Length + 1);
                nameValuesArray[nameValuesArray.Length - 1] =
                    new KeyValuePair<string, string[]>(name, new string[args.Count]);

                int i = 0;
                foreach (var arg in args)
                {
                    nameValuesArray[nameValuesArray.Length - 1].Value[i++] = arg.ToString().Replace(quote, string.Empty);
                }
            }

            return nameValuesArray;
        }

        private void FindCommands(KeyValuePair<string, string[]>[] namesAndArgs)
        {
            if (namesAndArgs == null || namesAndArgs.Length == 0)
                return;

            var commandArray = FilterTypes(Assembly.GetEntryAssembly().GetTypes());
            EnqueueAlwaysRunCommands(commandArray);
            if (namesAndArgs.Length == 1 && namesAndArgs[0].Key == All)
                EnqueueNotAlwaysRunCommands(commandArray);
            else EnqueueCommands(namesAndArgs, commandArray);
        }

        private KeyValuePair<CommandAttribute, Command>[] FilterTypes(Type[] types)
        {
            var commandArray = new KeyValuePair<CommandAttribute, Command>[0];
            CommandAttribute attribute = null;
            Command command = null;

            foreach (var type in types)
            {
                if (!TypeSelector(type))
                    continue;

                attribute = type.GetCustomAttribute<CommandAttribute>();

                if (!AttributeSelector(attribute))
                    continue;

                command = Activator.CreateInstance(type) as Command;

                if (!CommandSelector(command))
                    continue;

                Array.Resize(ref commandArray, commandArray.Length + 1);
                commandArray[commandArray.Length - 1] = new KeyValuePair<CommandAttribute, Command>(attribute, command);
            }

            return commandArray;
        }

        private void EnqueueAlwaysRunCommands(KeyValuePair<CommandAttribute, Command>[] commands)
        {
            var alwaysRun = from item in commands
                            where item.Key?.AlwaysRun == true
                            select item;

            foreach (var item in alwaysRun)
            {
                this.commands.Enqueue(new KeyValuePair<string, Command>(item.Key.Name, item.Value));
                SetupCommand(item.Value);
            }
        }

        private void EnqueueNotAlwaysRunCommands(KeyValuePair<CommandAttribute, Command>[] commands)
        {
            var notAlwasRun = from item in commands
                              where item.Key?.AlwaysRun == false
                              select item;

            foreach (var item in notAlwasRun)
            {
                this.commands.Enqueue(new KeyValuePair<string, Command>(item.Key.Name, item.Value));
                SetupCommand(item.Value);
            }
        }

        private void EnqueueCommands(KeyValuePair<string, string[]>[] namesAndArgs, KeyValuePair<CommandAttribute, Command>[] commands)
        {
            Command command = null;
            foreach (var nameArgsPair in namesAndArgs)
            {
                command = commands.FirstOrDefault((item) => item.Key.Name == nameArgsPair.Key).Value;
                if (command == default(Command))
                    continue;

                command = Activator.CreateInstance(command.GetType()) as Command;
                this.commands.Enqueue(new KeyValuePair<string, Command>(nameArgsPair.Key, command));
                SetupCommand(command, nameArgsPair.Value);
            }
        }
    }
}
