﻿using System;

namespace CommandSystem
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class CommandAttribute : Attribute
    {
        public string Name { get; protected set; }
        public bool AlwaysRun { get; set; } = false;

        public CommandAttribute(string name)
        {
            Name = name;
        }
    }
}
