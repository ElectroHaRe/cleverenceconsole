﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommandSystem
{
    public abstract class Command
    {
        protected virtual List<string> Args { get; private set; } = new List<string>();

        public Command() { }

        public string[] GetArgs() => Args.ToArray();

        public virtual void SetArgs(params string[] args)
        {
            if (args == null || args.Length == 0)
                return;

            Args.AddRange(args);
        }


        public abstract Task<bool> Run();
    }
}
